from django.shortcuts import render

profile_name = 'Hepzibah Smith'
bio_dict = [{'subject' : 'Birthday', 'value' : '01 Jan'},\
{'subject' : 'Gender', 'value' : 'Female'},\
{'subject' : 'Expertise', 'value' : 'Marketing, Collector, Public Speaking'},\
{'subject' : 'Description', 'value' : 'Antique expert. Experience as marketer for 10 years'},\
{'subject' : 'Email', 'value' : 'hello@smith.com'}]
expertise = {'Marketing', 'Collector', 'Public Speaking'}

# Create your views here.
def index(request):
    response = {'bio_dict': bio_dict, 'profile_name': profile_name}
    return render(request, 'desc_halamanprofile.html', response)