from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile_name, bio_dict
from django.http import HttpRequest
# Create your tests here.
class HalamanProfileUnitTest(TestCase):

    def test_halaman_profile_url_is_exist(self):
        response = Client().get('/halaman-profile/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_profile_using_index_func(self):
        found = resolve('/halaman-profile/')
        self.assertEqual(found.func, index)

    def test_profile_name_is_empty(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertFalse(len(profile_name) == 0)

    def test_halaman_profile_bio_dict(self):
		# Check whether bio_dict is not None Object
        self.assertIsNotNone(bio_dict)
       # Check whether bio_dict entry is less than 3 items
        self.assertTrue(len(bio_dict) >= 4)

         # Checking all element in family member
        for bio in bio_dict:
             # Name cannot be None
             self.assertIsNotNone(bio['subject'])
             self.assertIsNotNone(bio['value'])

             # Name cannot be integer
             self.assertIsNot(type(bio['value']), type(8))

    #def test_halaman_profile_bio_shown_in_page(self):
     #   request = HttpRequest()
      #  response = index(request)
       # html_response = response.content.decode('utf8')

#        Checking all family member is shown in page
        #for bio in bio_dict:
         #   self.assertIn('<td class="subject">' + bio['subject'] + '</td>', html_response)
          #  self.assertIn('<td class="value">' + bio['value'] + '</td>', html_response)
