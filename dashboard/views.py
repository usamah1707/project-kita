from django.shortcuts import render
from add_friend.models import Friends
from update_status.models import Todo
from halaman_profile.views import profile_name
#from blabla import bla

# Create your views here.
response = {}
def index(request):
	response['friends'] = Friends.objects.all().count();
	response['post'] = Todo.objects.all().count();
	#todo = Todo.objects.all()
	#response['data'] = sesuatu.objects.all()
	response['name'] = profile_name
	
	response['status'] = Todo.objects.last()
	html = 'dashboard/dashboard.html'
	return render(request, html, response)