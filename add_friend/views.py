from django.shortcuts import render
from django.http import HttpResponseRedirect
from urllib.request import urlopen
from urllib.error import URLError
from .forms import Message_Form
from .models import Friends


# Create your views here.
landing_page_content = "Usamah nashirulhaq"
response = {'author': "Usamah nashirulhaq"}
def index(request):
    message = Friends.objects.all()
    response['message_form'] = Message_Form
    response['message'] = message
    html = 'add_friend/add_friend.html'
    return render(request,html,response)

def add_friend(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] 
        response['url'] = request.POST['url'] 
        message = Friends(name=response['name'], url=response['url'])
        message.save()
        html ='add_friend/form_result.html'
        # return render(request, html, response)
        return HttpResponseRedirect('/add-friend/')
    else:
        return HttpResponseRedirect('/add-friend/')

def url_is_valid(url):
    try:
        thepage = urlopen(url)
    except URLError as e:
        return False
    else:
        return True
