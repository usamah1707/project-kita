from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'type' : 'text',
        'class': 'form-control',
        'placeholder': 'Masukan Nama...'
    }
    url = {
        'type' : 'url',
        'class': 'form-control',
        'placeholder': 'Masukan URL...'
    }

    name = forms.CharField(label='Name', required=True,
                           max_length=27,
                           widget=forms.TextInput(attrs=attrs)
                           )

    url = forms.URLField(label="URL", required=True,
                             widget=forms.TextInput(attrs=url)
                             )

