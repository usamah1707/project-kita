from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Todo
from .forms import Todo_Form



     # Create your tests here.
class AddStatusUnitTest(TestCase):

    def test_update_status_url_is_exist(self):
        response = Client().get('/update_status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_url_is_exist_using_index_func(self):
        found = resolve('/update_status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
            # Creating a new activity
        new_activity = Todo.objects.create(status='mengerjakan lab_5 ppw')

            # Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data ={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'],["This field is required."])

    def test_update_status_url_is_exist_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update_status/add_status', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_update_status_url_is_exist_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update_status/add_status', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
