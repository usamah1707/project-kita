from django.db import models

class Todo(models.Model):
    status = models.TextField(max_length = 500)
    created_date = models.DateTimeField(auto_now_add = True)

# Create your models here.
