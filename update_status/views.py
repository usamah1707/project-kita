from django.shortcuts import render
from django.http import HttpResponseRedirect
from halaman_profile.views import profile_name
from .forms import Todo_Form
from .models import Todo

# Create your views here.
response = {}
def index(request):
    todo = Todo.objects.order_by('-id')
    response['todo'] = todo
    html = 'update_status/index.html'
    response['namaku'] = profile_name
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_status(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        todo = Todo(status=response['status'])
        todo.save()
        return HttpResponseRedirect('/update_status/')
    else:
        return HttpResponseRedirect('/update_status/')
